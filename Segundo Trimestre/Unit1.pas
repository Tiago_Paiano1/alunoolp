unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Unit2, System.Net.URLClient,
  System.Net.HttpClient, System.Net.HttpClientComponent, Vcl.Menus, Vcl.StdCtrls;

type
  TForm1 = class(TForm)
    MainMenu1: TMainMenu;
    Memo1: TMemo;
    Button1: TButton;
    Navegador: TNetHTTPClient;
    Arquivo1: TMenuItem;
    AbrirFormulario1: TMenuItem;
    Dowload1: TMenuItem;
    procedure Dowload1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure AbrirFormulario1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  Form2: TForm2;

implementation

{$R *.dfm}



procedure TForm1.AbrirFormulario1Click(Sender: TObject);
begin
    Form2 := Tform2.Create(Application);
    Form2.Visible := True;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  Form2 := Tform2.Create(Application);
  Form2.Visible := True;
  end;

procedure TForm1.Dowload1Click(Sender: TObject);
var conteudo : string;
begin
   conteudo := Navegador.Get('https://venson.net.br').ContentAsString();
   Memo1.Text := conteudo;
end;

end.
